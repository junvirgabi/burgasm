package com.burgasm;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.burgasm.db.dbInvoice;
import com.burgasm.db.dbOrder;
import com.burgasm.model.Order;
import java.util.List;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Summary extends AppCompatActivity {

    TableLayout tl;
    dbInvoice invoices;
    dbOrder order;
    dbOrder orderSMS;
    List<Order> listOrder;
    RelativeLayout rl;
    TextView total;
    int sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        changeScreen();

        TextView notValid = new TextView(this);
        notValid.setText("NOT AVAILABLE ORDER/S: " + getIntent().getStringExtra("NOT_VALID"));
        notValid.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, R.id.tableLayout1);
        notValid.setLayoutParams(params);
        rl.addView(notValid);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendText();
//                sendSMS();
                Snackbar.make(view, "Sent Text", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
    protected void sendText(){

        orderSMS = new dbOrder(this);
        Intent getNo = getIntent();
        int counter = invoices.getInvoiceCount();
        listOrder = orderSMS.getAllOrderswith(counter);
        String number = getNo.getStringExtra("CELL_NUMBER");
        String sms = "";
        for(int x=0; x < listOrder.size();x++) {
            sms = listOrder.get(x).getProductId() +" \n "+String.valueOf(listOrder.get(x).getQuantity()) +" \n "+String.valueOf(listOrder.get(x).getPrice());
        }
//        try {
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(number, null, sms, null, null);
//            Toast.makeText(getApplicationContext(), "SMS Sent!",
//                    Toast.LENGTH_LONG).show();
//        } catch (Exception e) {
//
//            Toast.makeText(getApplicationContext(),
//                    "SMS faild, please try again later!",
//                    Toast.LENGTH_LONG).show();
//            e.printStackTrace();
//        }
        Uri uri = Uri.parse("smsto:" +number);
        Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
        smsIntent.putExtra("sms_body", "Hello /n"+sms);
//        try{
startActivity(smsIntent);
//        }catch(Exception e){
//            Toast.makeText(getApplicationContext(),
//                  "SMS faild, please try again later!",
//                  Toast.LENGTH_LONG).show();
//        }
    }

    public void changeScreen(){
        tl = (TableLayout) findViewById(R.id.tableLayout1);
        invoices = new dbInvoice(this);
        order = new dbOrder(this);
        total = (TextView) findViewById(R.id.total);
        sum = 0;
        int counter = invoices.getInvoiceCount();
        listOrder = order.getAllOrderswith(counter);
        rl = (RelativeLayout) findViewById(R.id.rl);

        for(int x = 0; x < listOrder.size(); x++) {

            TableRow tr = new TableRow(this);
            tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            TextView b = new TextView(this);
            b.setText(listOrder.get(x).getProductId());
            b.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            b.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            TextView c = new TextView(this);
            c.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            c.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            c.setText(String.valueOf(listOrder.get(x).getQuantity()));
            c.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            TextView d = new TextView(this);
            d.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            d.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            d.setText(String.valueOf(listOrder.get(x).getPrice()));
            sum += listOrder.get(x).getPrice();
            d.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            Button e = new Button(this);
            e.setText(R.string.cancel);
            e.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            final int deletion = x;
            e.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    order.deleteOrder(listOrder.get(deletion));
                    sum -= listOrder.get(deletion).getPrice();
                    tl.removeViews(1, listOrder.size());
                    changeScreen();
//                    Toast.makeText(getApplicationContext(), String.valueOf(deletion), Toast.LENGTH_SHORT).show();
//                    tl.removeViewAt(deletion+1);
                }
            });
            tr.addView(b);
            tr.addView(c);
            tr.addView(d);
            tr.addView(e);
            tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        }
        changeTotal();
    }
    public  void changeTotal(){
        total.setText(String.valueOf(sum));
    }

}
