package com.burgasm.control;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.burgasm.R;
import com.burgasm.Summary;
import com.burgasm.db.dbInvoice;
import com.burgasm.db.dbOrder;
import com.burgasm.db.dbProducts;
import com.burgasm.model.Invoice;
import com.burgasm.model.Order;
import com.burgasm.model.Products;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by junvir on 10/16/2015.
 */
public class OrderHandler {

    List<Products> products;
    Order order;
    dbOrder orderHandler;
    dbProducts productHandler;
    dbInvoice invoiceHandler;
    String[][] orderSplit;
    Context context;
    String notValid;
    String cellphoneNumber;
    boolean success;

    public OrderHandler(Context context) {
        productHandler = new dbProducts(context);
        invoiceHandler = new dbInvoice(context);
        orderHandler = new dbOrder(context);
        this.context = context;
    }

    public void handleOrder(String speech){
        notValid = "";
        divideOrder(speech);
        if(success) {
            int countProduct = 0;
            int countInvoice = invoiceHandler.getInvoiceCount();
            products = productHandler.getAllContacts();
            invoiceHandler.addInvoice(new Invoice(countInvoice, cellphoneNumber, 0));
            int x = 0;
            for (countProduct = orderSplit.length; x < countProduct; x++) {
                if (orderSplit[x][x] == null) {
                    break;
                }
                int z = 0;
                for ( z = 0; z < products.size(); z++) {
                    if (x == 0) {
                        if (products.get(z).getProductId().equalsIgnoreCase(orderSplit[x][1])) {
                            double price = products.get(z).getUnitPrice();
                            order = new Order(countProduct, orderSplit[x][1], Integer.parseInt(orderSplit[x][0]), price, countInvoice + 1);
                            orderHandler.addOrder(order);
                            break;
                        }
                    } else {
                        if (products.get(z).getProductId().equalsIgnoreCase(orderSplit[x][2])) {
                            double price = products.get(z).getUnitPrice();
                            order = new Order(countProduct, orderSplit[x][2], Integer.parseInt(orderSplit[x][1]), price, countInvoice + 1);
                            orderHandler.addOrder(order);
                            break;
                        }
                    }
                }
                if(z==products.size()) {
                    if (x == 0) {
                        notValid += orderSplit[x][1] + " ";
                    } else {
                        notValid += orderSplit[x][2] + " ";
                    }
                }
            }
            success = true;
        }
        else{
            return;
        }

    }

    public void divideOrder(String speech){
        Toast.makeText(context, speech, Toast.LENGTH_LONG).show();
//        speech = "1 BA + 2 BB + 3 BC + 4 AC";

        orderSplit = new String[100][100];
        String[] delPlus = speech.split("\\+");
        if(delPlus[0].split(" ").length
                == 1){
            Toast.makeText(context, R.string.orderFormat, Toast.LENGTH_SHORT).show();
            success = false;
        }
        else {
            String temp[] = null;
            for (int x = 0; x < delPlus.length; x++) {
                temp = delPlus[x].split(" ");
                for (int y = 0; y < temp.length; y++) {
                    orderSplit[x][y] = temp[y];
                }
            }
            success = true;
        }

    }

    public boolean isSuccess(){
        return success;
    }

    public void setCellphoneNumber(String cellphoneNumber){
        this.cellphoneNumber = cellphoneNumber;
    }

    public String getNotValid(){
        return notValid;
    }

}
