package com.burgasm;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.burgasm.adapter.CardAdapter;
import com.burgasm.control.OrderHandler;
import com.burgasm.db.dbInvoice;
import com.burgasm.db.dbOrder;
import com.burgasm.db.dbProducts;
import com.burgasm.model.Products;
import com.burgasm.recycler_model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CategoryActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    CardAdapter mAdapter;
    private static final int REQ_CODE_SPEECH_INPUT = 1001;
    dbOrder order;
    dbInvoice invoice;
    List<Products> products;
    dbProducts productHandler;
    OrderHandler oh;
    Toolbar toolbar;
    Intent i;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        oh = new OrderHandler(this);
        i = getIntent();
//        oh.setCellphoneNumber(i.getStringExtra("CELL_NUMBER"));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "wazzup", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                promptSpeechInput();
            }
        });

        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mAdapter = new CardAdapter(new ArrayList<Category>());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        addCategories();
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent
                .LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, getString(R.string.speech_not_supported), Toast.LENGTH_LONG).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQ_CODE_SPEECH_INPUT) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (result != null && !result.isEmpty()) {
                ArrayList<String> speech = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                oh.handleOrder(speech.get(0));
                oh.setCellphoneNumber(i.getStringExtra("CELL_NUMBER"));
                if(oh.isSuccess()){
                    Intent forSummary = new Intent(this, Summary.class);
                    forSummary.putExtra("CELL_NUMBER", i.getStringExtra("CELL_NUMBER"));
                    forSummary.putExtra("NOT_VALID", oh.getNotValid());
                    startActivity(forSummary);
                }
            }
        }
    }

    private void addCategories() {
        int icon = R.drawable.burgerfinal1;
        productHandler = new dbProducts(this);
        products = productHandler.getAllContacts();
        int size = products.size();
        for(int x = 0 ; x<size ; x++) {
            if(x<6){
            int y = x+1;
            Category category = new Category(products.get(x).getProductId(),products.get(x).getProduct_Content(), products.get(x).getUnitPrice(),icon+x);
            mAdapter.addCategory(category);}
            else if(x>5 && x <=8){
                Category category = new Category(products.get(x).getProductId(),products.get(x).getProduct_Content(), products.get(x).getUnitPrice(),R.drawable.softdrink);
                mAdapter.addCategory(category);
            }else if(x>8 && x <=11){
                Category category = new Category(products.get(x).getProductId(),products.get(x).getProduct_Content(), products.get(x).getUnitPrice(),R.drawable.fries);
                mAdapter.addCategory(category);
            }else{
                Category category = new Category(products.get(x).getProductId(),products.get(x).getProduct_Content(), products.get(x).getUnitPrice(),R.drawable.hotdog);
                mAdapter.addCategory(category);
            }

        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        switch (item.getItemId()) {
            case R.id.item1:
            Toast.makeText(this,"You selected to change churvaloo",Toast.LENGTH_SHORT).show();

        }
        return super.onOptionsItemSelected(item);
    }
}
