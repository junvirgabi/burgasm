package com.burgasm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Toast;

import com.burgasm.model.Products;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian on 10/11/2015.
 */
public class dbProducts extends SQLiteOpenHelper {

    // ---------------------------------------------------------------------------------------------
    // Database Settings
    // ---------------------------------------------------------------------------------------------
    private static final int    DB_VERSION = 1;
    private static final String DB_NAME    = "products.db";

    // ---------------------------------------------------------------------------------------------
    // Contacts Schema
    // ---------------------------------------------------------------------------------------------
    private static final String TABLE_PRODUCTS = "products";
    private static final String KEY_ID         = "product_id";
    private static final String KEY_PRICE      = "product_price";
    private static final String KEY_COMBO_CONTENT = "combo_content";

    private static final String[] PROJECTIONS_PRODUCTS = {KEY_ID, KEY_PRICE, KEY_COMBO_CONTENT};

    private static final int KEY_ID_INDEX       = 0;
    private static final int KEY_PRICE_INDEX     = 1;
    private static final int KEY_COMBO_CONTENT_INDEX = 2;

    // ---------------------------------------------------------------------------------------------
    // SQL STATEMENTS
    // ---------------------------------------------------------------------------------------------
    private static final String CREATE_TABLE_PRODUCTS = "CREATE TABLE IF NOT EXISTS products (" +
            KEY_ID + " TEXT PRIMARY KEY, " +
            KEY_PRICE + " NUMBER," +
            KEY_COMBO_CONTENT + " TEXT )";
    Context context;

    public dbProducts(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context=context;
        SQLiteDatabase sb = getReadableDatabase();
        if( getProductsCount() == 0) {
            addProduct(new Products("BA", 100, "Regular Burger with Softdrink"));
            addProduct(new Products("BB", 125, "Regular Burger with Fries and Softdrink"));
            addProduct(new Products("BC",150,"Big Burger with Softdrink and Fries"));
            addProduct(new Products("BD",175,"Superburger with Softdrink and Fries"));
            addProduct(new Products("BE",200,"Megaburger with Softdrink , Hotdog and Fries"));
            addProduct(new Products("BF",250,"Ultraburger with Softdrink, Fries and Hotdog"));
            addProduct(new Products("DA",10,"Small Drinks"));
            addProduct(new Products("DB",20,"Medium Drinks"));
            addProduct(new Products("DC",30,"Large Drinks"));
            addProduct(new Products("FA",15,"Small Fries"));
            addProduct(new Products("FB",30,"Medium Fries"));
            addProduct(new Products("FC",45,"Large Fries"));
            addProduct(new Products("HA",70,"Hot Dog"));
        }
        int counter = getProductsCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PRODUCTS);


    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTable(TABLE_PRODUCTS);
        onCreate(db);
    }

    // ---------------------------------------------------------------------------------------------
    // CRUD (Create, Read, Update, Delete) Operations
    // ---------------------------------------------------------------------------------------------
    public void addProduct(Products product) {
        if (product == null) {
            return;
        }
//
        SQLiteDatabase db = getWritableDatabase();

//
//
        if( db == null ){
            return;
        }
        ContentValues cv = new ContentValues();
        cv.put(KEY_ID, product.getProductId());
        cv.put(KEY_PRICE, product.getUnitPrice());
        cv.put(KEY_COMBO_CONTENT, product.getProduct_Content());
        // Inserting Row
        db.insert(TABLE_PRODUCTS, null, cv);


        db.close();
    }

    public Products getProducts(int id) {
        SQLiteDatabase db = getReadableDatabase();

        if (db == null) {
            return null;
        }

        Cursor cursor = db.query(TABLE_PRODUCTS, PROJECTIONS_PRODUCTS, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (!cursor.moveToFirst()) {
            return null;
        }

        Products product = new Products(cursor.getString(KEY_ID_INDEX), cursor.getDouble(KEY_PRICE_INDEX),
                cursor.getString(KEY_COMBO_CONTENT_INDEX));

        cursor.close();

        return product;
    }

    public double getPrice (String code){
        String selectQuery = "Select product_price from products where product_id="+"'"+code.toUpperCase()+"'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        double price=0.0;
        if(cursor.moveToFirst()){
            do{
                price = cursor.getDouble(KEY_PRICE_INDEX);
            }while(cursor.moveToNext());
        }
        cursor.close();
        return price;
    }

    public List<Products> getAllContacts() {
        List<Products> products = new ArrayList<>();

        // Selection
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(KEY_ID_INDEX);
                double price = cursor.getDouble(KEY_PRICE_INDEX);
                String burger = cursor.getString(KEY_COMBO_CONTENT_INDEX);
                Products product = new Products(id, price, burger);
                products.add(product);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return products;
    }

    public int updateProduct(Products product) {
        if (product == null) {
            return -1;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return -1;
        }

        ContentValues cv = new ContentValues();
        cv.put(KEY_PRICE, product.getUnitPrice());
        cv.put(KEY_COMBO_CONTENT, product.getProduct_Content());


        // Upating the row
        int rowCount = db.update(TABLE_PRODUCTS, cv, KEY_ID + "=?",
                new String[]{String.valueOf(product.getProductId())});

        db.close();

        return rowCount;
    }

    public void deleteProduct(Products product) {
        if (product == null) {
            return;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return;
        }

        db.delete(TABLE_PRODUCTS, KEY_ID + "=?", new String[]{String.valueOf(product.getProductId())});
        db.close();
    }

    public int getProductsCount() {
        String query = "SELECT * FROM  " + TABLE_PRODUCTS+";";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();
//
        cursor.close();
//        Toast.makeText(context, String.valueOf(count), Toast.LENGTH_LONG).show();
        return count;
    }

    public void dropTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();

        if (db == null || TextUtils.isEmpty(tableName)) {
            return;
        }
        Toast.makeText(context, "Table DROPPED", Toast.LENGTH_LONG).show();
    }
}

