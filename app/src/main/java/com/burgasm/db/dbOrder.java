package com.burgasm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.burgasm.model.Invoice;
import com.burgasm.model.Order;

/**
 * Created by Christian on 10/11/2015.
 */
public class dbOrder extends SQLiteOpenHelper {

    // ---------------------------------------------------------------------------------------------
    // Database Settings
    // ---------------------------------------------------------------------------------------------
    private static final int    DB_VERSION = 1;
    private static final String DB_NAME    = "order.db";

    // ---------------------------------------------------------------------------------------------
    // Contacts Schema
    // ---------------------------------------------------------------------------------------------
    private static final String TABLE_ORDER = "orders";
    private static final String KEY_ID         = "order_id";
    private static final String KEY_PRODUCT_ID         = "product_id";
    private static final String KEY_QUANTITY     = "order_quantity";
    private static final String KEY_PRICE     = "price";
    private static final String KEY_INVOICE_ID   = "invoice_id";


    private static final String[] PROJECTIONS_ORDER = {KEY_ID,KEY_PRODUCT_ID, KEY_QUANTITY, KEY_PRICE,KEY_INVOICE_ID};

    private static final int KEY_ID_INDEX       = 0;
    private static final int KEY_PRODUCT_ID_INDEX     = 1;
    private static final int KEY_QUANTITY_INDEX    = 2;
    private static final int KEY_PRICE_INDEX = 3;
    private static final int KEY_INVOICE_ID_INDEX = 4;

    // ---------------------------------------------------------------------------------------------
    // SQL STATEMENTS
    // ---------------------------------------------------------------------------------------------
    private static final String CREATE_TABLE_ORDER = "CREATE TABLE IF NOT EXISTS orders(" +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_PRODUCT_ID +" TEXT, "+
            KEY_QUANTITY + " NUMBER, " +
            KEY_PRICE + " NUMBER, " +
            KEY_INVOICE_ID + " NUMBER, FOREIGN KEY (invoice_id) REFERENCES invoice(invoice_id), "+
            " FOREIGN KEY (product_id) REFERENCES products(product_id))";

    Context context;
    public dbOrder(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        SQLiteDatabase sb = getReadableDatabase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ORDER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTable(TABLE_ORDER);
        onCreate(db);
    }

    // ---------------------------------------------------------------------------------------------
    // CRUD (Create, Read, Update, Delete) Operations
    // ---------------------------------------------------------------------------------------------
    public void addOrder(Order order) {
        if (order == null) {
            return;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return;
        }

        ContentValues cv = new ContentValues();
        cv.put(KEY_PRODUCT_ID, order.getProductId());
        cv.put(KEY_QUANTITY, order.getQuantity());
        cv.put(KEY_PRICE, order.getPrice());
        cv.put(KEY_INVOICE_ID, order.getInvoiceId());
        // Inserting Row
        db.insert(TABLE_ORDER, null, cv);
        db.close();
    }

    public List<Order> getAllOrderswith(int invoice_id){
        List<Order> orders = new ArrayList<>();

        // Selection
        String selectQuery = "SELECT * FROM " + TABLE_ORDER + " where invoice_id="+invoice_id;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
//            Toast.makeText(context, "CHAN GWAPO", Toast.LENGTH_LONG).show();
            do {
                int id = cursor.getInt(KEY_ID_INDEX);
                String product_id = cursor.getString(KEY_PRODUCT_ID_INDEX);
                int quantity = cursor.getInt(KEY_QUANTITY_INDEX);
                double price = cursor.getDouble(KEY_PRICE_INDEX);
                int invoice_ids = cursor.getInt(KEY_INVOICE_ID_INDEX);
                Order order = new Order(id,product_id,quantity,price/quantity,invoice_ids);
                orders.add(order);

            } while (cursor.moveToNext());
        }

        cursor.close();

        return orders;

    }


    public Order getOrder(int id) {
        SQLiteDatabase db = getReadableDatabase();

        if (db == null) {
            return null;
        }

        Cursor cursor = db.query(TABLE_ORDER, PROJECTIONS_ORDER, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (!cursor.moveToFirst()) {
            return null;
        }

        Order order = new Order(cursor.getInt(KEY_ID_INDEX),cursor.getString(KEY_PRODUCT_ID_INDEX), cursor.getInt(KEY_QUANTITY_INDEX),
                cursor.getDouble(KEY_PRICE_INDEX), cursor.getInt(KEY_INVOICE_ID_INDEX));

        cursor.close();

        return order;
    }

    public List<Order> getAllOrder() {
        List<Order> orders = new ArrayList<>();

        // Selection
        String selectQuery = "SELECT * FROM " + TABLE_ORDER;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(KEY_ID_INDEX);
                String product_id = cursor.getString(KEY_PRODUCT_ID_INDEX);
                int quantity = cursor.getInt(KEY_QUANTITY_INDEX);
                double price = cursor.getDouble(KEY_PRODUCT_ID_INDEX);
                int invoice_id = cursor.getInt(KEY_INVOICE_ID_INDEX);
                Order order = new Order(id,product_id,quantity,price,invoice_id);
                orders.add(order);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return orders;
    }

    public int updateOrder(Order order) {
        if (order == null) {
            return -1;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return -1;
        }

        ContentValues cv = new ContentValues();
        cv.put(KEY_PRODUCT_ID, order.getProductId());
        cv.put(KEY_QUANTITY, order.getQuantity());
        cv.put(KEY_PRICE, order.getPrice());
        cv.put(KEY_INVOICE_ID, order.getInvoiceId());
        // Upating the row
        int rowCount = db.update(TABLE_ORDER, cv, KEY_ID + "=?",
                new String[]{String.valueOf(order.getOrderId())});

        db.close();

        return rowCount;
    }

    public void deleteOrder(Order order) {
        if (order == null) {
            return;
        }

        SQLiteDatabase db = this.getWritableDatabase();

        if (db == null) {
            return;
        }

        db.delete(TABLE_ORDER, KEY_ID + "=?", new String[]{String.valueOf(order.getOrderId())});
        db.close();
    }

    public int getOrderCount() {
        String query = "SELECT * FROM  " + TABLE_ORDER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        cursor.close();

        return count;
    }

    public void dropTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();

        if (db == null || TextUtils.isEmpty(tableName)) {
            return;
        }
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER);
    }
}
