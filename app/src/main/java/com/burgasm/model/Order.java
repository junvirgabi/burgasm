package com.burgasm.model;

import com.burgasm.db.dbProducts;

/**
 * Created by Christian on 10/11/2015.
 */
public class Order {
    private int OrderId;
    private String ProductId;
    private int Quantity;
    private double Price;
    private int InvoiceId;
    dbProducts productHandler;

    public Order(int orderId, String productId, int quantity, double price, int invoiceId) {
        OrderId = orderId;
        ProductId = productId;
        Quantity = quantity;
        Price = Quantity*price;
        InvoiceId = invoiceId;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        InvoiceId = invoiceId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "OrderId=" + OrderId +
                ", ProductId='" + ProductId + '\'' +
                ", Quantity=" + Quantity +
                ", Price=" + Price +
                ", InvoiceId=" + InvoiceId +
                '}';
    }
}
