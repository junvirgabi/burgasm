package com.burgasm.model;

/**
 * Created by Christian on 10/16/2015.
 */
public class Invoice {
    private int InvoiceId;
    private String UserNumber;
    private double Totalprice;

    public Invoice(int invoiceId, String userNumber, double totalprice) {
        InvoiceId = invoiceId;
        UserNumber = userNumber;
        Totalprice = totalprice;
    }

    public int getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        InvoiceId = invoiceId;
    }

    public String getUserNumber() {
        return UserNumber;
    }

    public void setUserNumber(String userNumber) {
        UserNumber = userNumber;
    }

    public double getTotalprice() {
        return Totalprice;
    }

    public void setTotalprice(double totalprice) {
        Totalprice = totalprice;
    }
}
