package com.burgasm.model;

/**
 * Created by Christian on 10/11/2015.
 */
public class Products {
    private String ProductId;
    private double UnitPrice;
    private String Product_Content;

    public Products(String productId, double unitPrice, String product_Content) {
        ProductId = productId;
        UnitPrice = unitPrice;
        Product_Content = product_Content;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public double getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getProduct_Content() {
        return Product_Content;
    }

    public void setProduct_Content(String product_Content) {
        Product_Content = product_Content;
    }
}
